## 推理引擎ESNN使用方法(本机运行)

1. 编译工程

```
mkdir build
cd build
cmake ..
make
```

2. 拷贝自定义格式的神经网络视觉分类模型和测试图到指定文件夹下

```
cp 根目录/examples/1_4000_3.jpg 根目录/examples/modelres6 根目录/examples/paramres6  根目录/build/examples/
```

3. 运行酒花分类例子

```
cd 根目录/build/examples
./classfication paramres6 modelres6 1_4000_3.jpg
```

若成功推理模型，会依次输出网络结构信息、前传时间、不同分类概率、最大概率类别。

在本人mac上前传时间大约为558.61000000ms

对于该酒花分类模型来说，0~5标签分别代表头酒、二段酒、二段转三段酒、三段酒、三段转酒尾酒、酒尾；
而测试用的图片1_4000_3.jpg 、2_1630_3.jpg、 真实分类分别为二段酒、三段酒、酒尾。

测试图片1_4000_3.jpg，最大概率类别为1，即“二段酒”分类标签，概率为0.904913；
测试图片2_1630_3.jpg，最大概率类别为3，即“三段酒”分类标签，概率为0.895081；
测试图片2_5995_2.jpg，最大概率类别为5，即“酒尾”分类标签，概率为0.998537。

## LICENSE
Licensed under the MIT license
